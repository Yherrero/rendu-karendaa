# カレンダー

カレンダー (karendaa) is a calendar app where you can manage your events and your calendars.

links :

- [Karendaa client](https://karenda.netlify.com/)
- [Karendaa api](https://karendaa-api.herokuapp.com/)
- [Karendaa docs](https://karendaa-api-docs.netlify.com/)

Readme :

- [Readme api](https://gitlab.com/Yherrero/rendu-karendaa/blob/master/server/README.MD)
- [Readme client](https://gitlab.com/Yherrero/rendu-karendaa/blob/master/client/README.md)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

There are some global dependencies you need to set up.

- API :
  - [Node.js](https://nodejs.org/)
  - [Vuepress](https://github.com/vuejs/vuepress) (documentation)

### Installing

```sh
git clone https://gitlab.com/Yherrero/karendaa-v2.git
```

API :

```sh
cd server/
npm i
# dev :
npm run dev
# production :
npm start
```

API-DOCS :

```sh
cd api-docs/
vuepress dev
# to build the doc
vuepress build
```

CLIENT :

```sh
cd client/
npm i
npm run serve
# build :
npm run build
```

## How to use the API

Go to [docs](https://karendaa-api-docs.netlify.com/) to get more details.

## Built With

- [Node.js](https://nodejs.org/)
- [Vuepress](https://github.com/vuejs/vuepress)
- [Vuepress-theme-api](https://github.com/sqrthree/vuepress-theme-api)
- [Vue js](https://vuejs.org/)

## Authors

- [**Yannick Herrero**](https://yannick-herrero.me)
- [**David Seynaeve**](http://david-sey.fr)

## License

This project is licensed under the ISC License
