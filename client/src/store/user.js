import axios from 'axios'
import jwtDecode from 'jwt-decode'

export default {
  state: {
    token: localStorage.getItem('token') || '',
    currentCalendar: 0,
    user: null,
    loadUserWaiting: false
  },
  mutations: {
    editLoadUserWaiting(state, payload) {
      state.loadUserWaiting = payload
    },
    authSuccess(state, payload) {
      state.token = payload.token
    },
    logout(state) {
      state.token = ''
      state.user = null
    },
    loadUser(state, payload) {
      state.user = payload
    },
    changeCurrentCalendar(state, payload) {
      state.currentCalendar = payload
    },
    createEvent(state, payload) {
      state.user.calendars
        .find(c => c._id === payload.currentCalendar)
        .events.push(payload.event)
    },
    createCalendar(state, payload) {
      state.user.calendars.push(payload.calendar)
      state.currentCalendar = payload.calendar._id
    },
    editEvent(state, payload) {
      const event = state.user.calendars
        .find(c => c._id === state.currentCalendar)
        .events.find(e => e._id === payload._id)
      event.title = payload.title
      event.description = payload.description
      event.start = payload.start
      event.end = payload.end
    },
    deleteEvent(state, payload) {
      const calendar = state.user.calendars.find(
        c => c._id === state.currentCalendar
      )
      calendar.events = calendar.events.filter(e => e._id !== +payload)
    }
  },
  actions: {
    register({ commit, dispatch }, user) {
      return new Promise((resolve, reject) => {
        axios
          .post('/users/signup', user)
          .then(res => {
            dispatch('login', user)
            resolve(res)
          })
          .catch(err => {
            localStorage.removeItem('token')
            reject(err)
          })
      })
    },
    login({ commit, dispatch }, user) {
      return new Promise((resolve, reject) => {
        axios
          .post('/users/login', user)
          .then(res => {
            const token = 'bearer ' + res.data.token
            localStorage.setItem('token', token)
            axios.defaults.headers.common['Authorization'] = token
            const decoded = jwtDecode(token)
            const user = {
              _id: decoded.id,
              email: decoded.email
            }
            commit('authSuccess', { token, user })
            dispatch('loadUser')
            resolve(res)
          })
          .catch(err => {
            localStorage.removeItem('token')
            reject(err)
          })
      })
    },
    logout({ commit }) {
      return new Promise((resolve, reject) => {
        commit('logout')
        localStorage.removeItem('token')
        delete axios.defaults.headers.common['Authorization']
        resolve()
      })
    },
    loadUser({ commit, getters }) {
      commit('editLoadUserWaiting', true)
      return new Promise((resolve, reject) => {
        axios
          .get('/users/')
          .then(res => {
            commit('loadUser', res.data.user)
            const calendars = getters.allCalendars
            if (calendars.length) {
              commit('changeCurrentCalendar', calendars[0].id)
            }
            commit('editLoadUserWaiting', false)
            resolve()
          })
          .catch(err => reject(err.response))
      })
    },
    changeCurrentCalendar({ commit }, id) {
      commit('changeCurrentCalendar', id)
    },
    createEvent({ commit, getters }, event) {
      return new Promise((resolve, reject) => {
        const currentCalendar = getters.currentCalendar
        event.calendarId = currentCalendar
        axios
          .post('/events', event)
          .then(res => {
            commit('createEvent', { currentCalendar, event: res.data.newEvent })
            resolve()
          })
          .catch(err => reject(err.response.data.errors))
      })
    },
    createCalendar({ commit, getters }, calendar) {
      axios
        .post('/calendars', calendar)
        .then(res => {
          commit('createCalendar', { calendar: res.data.newCalendar })
        })
        .catch(err => console.log(err.response.data))
    },
    editEvent({ commit }, event) {
      return new Promise((resolve, reject) => {
        axios
          .patch(`/events/${event._id}`, event)
          .then(res => {
            commit('editEvent', event)
            resolve()
          })
          .catch(err => reject(err))
      })
    },
    deleteEvent({ commit }, eventId) {
      return new Promise((resolve, reject) => {
        axios
          .delete('/events/' + eventId)
          .then(res => {
            resolve(res)
            commit('deleteEvent', eventId)
          })
          .catch(err => reject(err))
      })
    }
  },
  getters: {
    loadUserWaiting: state => state.loadUserWaiting,
    loggedIn: state => !!state.token,
    currentCalendar: state => state.currentCalendar,
    calendarColor(state, getters) {
      if (
        state.user === null ||
        state.user.calendars[getters.currentCalendar] === undefined
      ) {
        return '#bada55'
      }
      return state.user.calendars[getters.currentCalendar].color || '#bada55'
    },
    allCalendars(state) {
      if (state.user === null) {
        return []
      }
      return state.user.calendars.map(c => {
        return {
          title: c.title,
          color: c.color,
          id: c._id
        }
      })
    },
    calendarsCount: (state, getters) => getters.allCalendars.length,
    allEvents(state, getters) {
      if (state.user === null) return []
      const calendar = state.user.calendars.find(
        calendar => calendar._id === getters.currentCalendar
      )
      if (calendar === undefined) return []
      return calendar.events || []
    }
  }
}
