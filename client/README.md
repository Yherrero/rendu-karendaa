# Karendaa - client

(Auteurs : **Yannick Herrero** & **David Seynaeve**)

Cette partie présente les différentes informations concernant la partie frontend en Vue.js.
[Lien de l'application](https://karenda.netlify.com/)

## Sommaire

- [Getting started](#getting-started)
- [Fonctionalités](#fonctionalités)
- [Développement](#développement)
  - [Organisation](#organisation-)
  - [Gestion des données et store](#gestion-de-l'êtat-et-store-)
  - [Routes](#routes-)
  - [Principaux composants](#principaux-composants-)
  - [Connexion avec l'api](#connexion-avec-l'api-)
  - [Modules externes](#modules-externes-)

## Getting started

une fois dans le dossier client :

```sh
# pour l'installation
npm i
# pour lancer l'application en mode dev
npm run serve
# pour lancer le build de l'application
npm run build
```

## Fonctionalités

L'application カレンダー (karendaa) permet à un utilisateur de gérer ses evenements dans un ou plusieurs calendriers.  
Un utilisateur peut créer autant de calendriers qui contiennent chacuns leurs propres évenements liés. Par exemple, il peut noter tout les anniversaires de ses contacts dans un calendrier "Birthdays", gérer tout ses prochains meetups dans un calendrier "Meetups" tout en notant ses échances dans un calendrier "Freelance".  
L'application propose donc à l'utilisateur la liste de ses calendriers et une fois un calendrier selectionné, il pourra avoir une vue annuelle, mensuelle ou hebdomadaire de ce calendrier.

- **Vue annuelle** : La vue annuelle permet d'avoir une vue d'ensemble sur ses évenements. Il sont regroupés par jours et l'on peut voir le détails des évenements d'une journée en passant la souris au dessus.
- **Vue mensuelle** : La vue mensuelle est plus précise. Elle permet de voir directement les différents évenements de chaque journées. Au survol d'un évenement, on peut accéder au détails de celui-ci.
- **Vue hebdomadaire** : La vue hebdomadaire est la plus précise. Elle permet de voir en détail les heures de chaque évenements dans une journée. On peut ici cliquer sur un évenement pour accéder aux détails de celui-ci.

Enfin, une fois que l'on accéde aux détails d'un évenement, on peut modifier son titre ou sa description en double cliquant dessus. On peut aussi supprimer un évenement en cliquant sur le bouton prévu à cet effet.

## Développement

### Organisation :

Chaque partie du code est séparée dans un dossier spécifique pour plus de clarté selon l'arborescence suivante :

```
/src
   |--/assets
      |--/js
        |--/myDate.js
   |--/components
   |--/layouts
   |--/views
   |--/store
   |--App.vue
   |--router.js
   |--main.js
```

- **App.vue** : Le point d'entrée de l'application
- **myDate.js** : myDate est une classe qui permet de gérer tout les calendriers présents sur le site. Elle permet de générer des tableaux de dates en fonction d'un mois, d'une année etc... C'est la classe centrale de l'application qui permet de garder tout les vues de calendrier synchronisés entre elles à l'aide du store.
- **components** : Dossier contenant les composants qui ne sont ni des views, ni des layouts.
- **layouts** : Dossier contenant les différents layouts de l'application. Il y a trois layouts différents en fonction de la route et de l'êtat de l'application.
- **views** : Dossier contenant les différentes vues de l'application. Elle représentent les composants principaux des routes différentes de l'application.
- **store** : Le store représente l'êtat de l'application. Il est divisé en trois différentes parties (date, shared et user) qui gèrent chacune une partie des données de l'application.
- **router.js** : Le router permet de gérer les composants à afficher en fonction de la route. Il gére aussi la redirection en fonction du droit de l'utilisateur à accéder a certaine pages (s'il est connecté ou simple visiteur)

### Gestion de l'êtat et store :

L'êtat de l'application est géré par le store. Cela permet d'accéder facilement à certaines données comme par exemple l'utilisateur connecté, la date actuelle sélectionnée par l'utilisateur ou encore la liste des calendriers et des évenements.  
La centralisation des données dans le store permet d'y accéder de n'importe ou et de ne pas avoir de problèmes de désynchronisation entre les différents composants. Il y a une seule source de vérité qui distribue des données immutable grâce à des getters et qui gére les modifications de ces données grâce à des actions prédéfinies.

### Routes :

Les différentes routes de l'applications sont les suivantes :

```
- /             - (connecté) - Vue principale du calendrier
- /home         - (visiteur) - Landing page
- /event/add    - (connecté) - Permet de créer un nouvel évenement
- /event/:id    - (connecté) - Permet de voir les détails d'un évenement
- /login        - (visiteur) - Page de connexion
- /signup       - (visiteur) - Page d'inscription
```

Note :

- les pages de login et de signup sont utiles que sur mobile, autrement il y a sur la landing page des formulaires sous forme de pop up qui permettent de réaliser ses actions.
- La page /event/add n'est pas vraiment utilisée car il y a un formulaire qui apparait en pop up pour créer de nouveaux évenements à la volée.

### Principaux composants :

Les composants sont nombreux (31) mais on peut les ranger dans différentes catégories :

- **App.vue (1)** : Le composant principale de l'application qui distribue les composants en fonction de l'êtat de l'application et de la route empruntée par l'utilisateur.
- **Les layouts (3)** : ils permettent de gérer le layout globale de la page en fonction de la route et de si l'utilisateur est connecté ou non. Les différents layouts affichent ou non la sidebar, le header simple ou le header complet.  
  Le layout est choisi dans le composant principale App.vue.
- **Les vues (6)** : Les vues sont les composants principaux qui représentent les routes de l'application (par exemple, la route /signup mène au composant Signup.vue)
- **Les autres composants (21)** : Ces composants représentent chaques parties de l'application, comme par exemple le header ou encore le datePicker qui permet de choisir des dates. Ils peuvent être plus ou moins grand et englober d'autres composants.

### Connexion avec l'api :

La connexion avec l'api est gérée grace au module axios. Les différents appels aux serveur sont gérés de tel sorte a améliorer au maximum les performances de l'application. Par exemple lors de l'ajout ou de la suppression d'un évenement ou d'un calendrier, une seule requete est envoyée au serveur. tout le reste est géré directement dans le state de l'application avec le store.

### Modules externes :

L'application à été faite de tel sorte à n'utiliser que le minimum de modules externes. Le but de ce développement étant de progresser dans le développement d'application en vue.js, il nous à semblé plus interessant de créer nos propres modules ou classes plutot que d'utiliser des librairies comme moment.js ou des datepicker déjà existants par exemple. C'est dans cet êtat d'esprit que nous avons développer plusieurs components ainsi que la classe myDate pour qu'ils soient assez génériques tout en répondant le plus possibles aux besoins de notre application.

Quelques modules et librairies externes ont tout de même étaient utilisées :

- **axios** pour les appels à l'api
- **jwt-decode** pour récupérer les informations du token de connexion
- **vue-router** pour la partie routage et spa
- **vuex** pour la gestion du state de l'application
