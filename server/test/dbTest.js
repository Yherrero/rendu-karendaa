const assert = require('chai').assert
const expect = require('chai').expect
const db = require('../api/models/db')

describe('db', () => {
  describe('getUser', () => {
    it('should return user 0', () => {
      const expected = {
        _id: 0,
        email: '1@mail.com',
        password: '123',
        calendars: []
      }
      expect(db.getUser(0)).to.deep.equal(expected)
    })
    it('should return user 1', () => {
      const expected = {
        _id: 1,
        email: '2@mail.com',
        password: '123',
        calendars: []
      }
      expect(db.getUser(1)).to.deep.equal(expected)
    })
    it('should return null', () => {
      assert.equal(db.getUser(1000), null)
    })
  })
  describe('getUserByEmail', () => {
    it('should return user 0', () => {
      const expected = {
        _id: 0,
        email: '1@mail.com',
        password: '123',
        calendars: []
      }
      expect(db.getUserByEmail('1@mail.com')).to.deep.equal(expected)
    })
    it('should return user 1', () => {
      const expected = {
        _id: 1,
        email: '2@mail.com',
        password: '123',
        calendars: []
      }
      expect(db.getUserByEmail('2@mail.com')).to.deep.equal(expected)
    })
    it('should return null', () => {
      assert.equal(db.getUserByEmail(1000), null)
    })
  })
  describe('getEvents', () => {
    it('should return all events of user 0', () => {
      expect(db.getEvents(0)).to.deep.equal([])
    })
  })
})
