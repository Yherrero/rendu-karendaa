const express = require('express')
// const morgan = require('morgan')
const cors = require('cors')
require('dotenv').config()

const calendarRoutes = require('./api/routes/calendars')
const eventRoutes = require('./api/routes/events')
const userRoutes = require('./api/routes/users')

const app = express()

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
// app.use(morgan('dev'))
app.use(cors())

app.use('/calendars', calendarRoutes)
app.use('/events', eventRoutes)
app.use('/users', userRoutes)

app.get('/api-docs', (req, res) =>
  res.redirect('https://karendaa-api-docs.netlify.com')
)

const port = process.env.PORT || 8080

app.listen(port, () => {
  console.log(`Server started on port ${port}`)
})
