const express = require('express')

const EventsController = require('../controllers/events')
const checkAuth = require('../middlewares/check-auth')

const router = express.Router()

/**
 * Get all event from logged user
 *
 */
router.get('', checkAuth, EventsController.get_all_events)

/**
 * Get one event by id
 *
 */
router.get('/:eventId', checkAuth, EventsController.get_event_by_id)

/**
 * Post a new event
 *
 */
router.post('', checkAuth, EventsController.create_event)

/**
 * Edit an event
 *
 */
router.patch('/:eventId', checkAuth, EventsController.edit_event_by_id)

/**
 * Delete an event
 *
 */
router.delete('/:eventId', checkAuth, EventsController.delete_event_by_id)

module.exports = router
