const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const config = require('../assets/config')
const db = require('../models/db')

/**
 * Create a new user
 *
 * @param {string} email
 * @param {string} password
 */
exports.signup_user = (req, res) => {
  const email = req.body.email
  const password = req.body.password

  // is all data entered ?
  if (!email || !password) {
    return res.status(config.errors.missingData.status).json({
      message: config.errors.missingData.message
    })
  }

  bcrypt.hash(password, 10, (err, hash) => {
    // fail with bcrypt
    if (err) {
      return res.status(500).json({
        message: err
      })
    } else {
      const errors = db.createUser(email, hash)

      if (errors.length) {
        res.status(config.errors.invalidData.status).json({
          message: config.errors.invalidData.message,
          errors
        })
      } else {
        const newUser = db.getLastUser()
        res.status(200).json({
          newUser
        })
      }
    }
  })
}

/**
 * Log a user in
 *
 * @param {string} email
 * @param {string} password
 */
exports.login_user = (req, res) => {
  const email = req.body.email
  const password = req.body.password
  const user = db.getUserByEmailLight(email)

  // Is email exists ?
  if (!user) {
    return res.status(config.errors.authFailed.status).json({
      message: config.errors.authFailed.message
    })
  }

  bcrypt.compare(password, user.password, (err, result) => {
    // fail with bcrypt
    if (err) {
      return res.status(config.errors.authFailed.status).json({
        message: config.errors.authFailed.message
      })
    }
    if (result) {
      const token = jwt.sign(
        {
          email: user.email,
          id: user._id
        },
        process.env.JWT_KEY,
        {
          expiresIn: '1h'
        }
      )

      return res.status(200).json({
        message: 'Auth successful',
        token
      })
    }
    // Is password corresponding ?
    return res.status(config.errors.authFailed.status).json({
      message: config.errors.authFailed.message
    })
  })
}

/**
 * Get all user infos
 *
 */
exports.retrieve_user = (req, res) => {
  const user = db.getUser(req.user._id)

  if (!user) {
    return res.status(config.errors.wrongUserID.status).json({
      message: config.errors.wrongUserID.message
    })
  }

  res.status(200).json({
    user
  })
}

// == /!\ == Only for testing purposes
// exports.get_all_users = (req, res) => {
//   res.status(200).json({
//     users: db.users
//   })
// }
