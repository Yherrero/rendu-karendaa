const config = require('../assets/config')
const db = require('../models/db')

/**
 * Create a new calendar for logged user
 *
 * @param {string} title
 * @param {string} [color]
 */
exports.create_calendar = (req, res) => {
  const title = req.body.title
  const color = req.body.color
  const userId = req.user._id

  if (!title) {
    return res.status(config.errors.missingData.status).json({
      message: config.errors.missingData.message
    })
  }

  const errors = !color
    ? db.createCalendar(userId, title)
    : db.createCalendar(userId, title, color)

  if (errors.length) {
    res.status(config.errors.invalidData.status).json({
      message: config.errors.invalidData.message,
      errors
    })
  } else {
    const newCalendar = db.getLastCalendar()
    res.status(200).json({
      newCalendar
    })
  }
}

/**
 * Edit a calendar
 *
 * @param {string} title
 * @param {string} color
 */
exports.edit_calendar_by_id = (req, res) => {
  const title = req.body.title
  const color = req.body.color
  const calendarId = req.params.calendarId
  const userId = req.user._id

  if (!title || !color) {
    return res.status(config.errors.missingData.status).json({
      message: config.errors.missingData.message
    })
  }

  const errors = db.editCalendar(userId, calendarId, title, color)

  if (errors.length) {
    res.status(config.errors.invalidData.status).json({
      message: config.errors.invalidData.message,
      errors
    })
  } else {
    res.status(200).json({
      message: 'Calendar edited'
    })
  }
}

/**
 * Delete a calendar
 *
 */
exports.delete_calendar_by_id = (req, res) => {
  const userId = req.user._id
  const calendarId = req.params.calendarId

  if (!calendarId) {
    return res.status(config.errors.missingData.status).json({
      message: config.errors.missingData.message
    })
  }

  db.deleteCalendar(userId, calendarId)

  res.status(200).json({
    message: 'Calendar deleted'
  })
}
