---
title: Overview
---

<Block>

</Block>

<Block>

## Current Version

By default, all requests to `https://karendaa-api.herokuapp.com` receive the v2 version of the REST API.

</Block>

<Block>

## Content Type

All requests must be encoded as JSON with the `Content-Type: application/json` header. Most responses, including errors, are encoded exclusively as JSON as well.

<Example>

```
Content-Type: application/json
```

</Example>

</Block>

<Block>

## Authentication

Provide your API token as part of the Authorization header.

If the authentication is unsuccessful, the status code **401** is returned.

<Example>

```
Authorization: Bearer $TOKEN
```

</Example>

</Block>

<Block>

## HTTP Verbs

Where possible, API strives to use appropriate HTTP verbs for each action.

|  Verb  |                                                                                                                                              Description                                                                                                                                               |
| :----: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|  HEAD  |                                                                                                                  Can be issued against any resource to get just the HTTP header info.                                                                                                                  |
|  GET   |                                                                                                                                     Used for retrieving resources.                                                                                                                                     |
|  POST  |                                                                                                                                      Used for creating resources.                                                                                                                                      |
| PATCH  | Used for updating resources with partial JSON data. For instance, an Issue resource has title and body attributes. A PATCH request may accept one or more of the attributes to update the resource. PATCH is a relatively new and uncommon HTTP verb, so resource endpoints also accept POST requests. |
|  PUT   |                                                                                Used for replacing resources or collections. For PUT requests with no body attribute, be sure to set the Content-Length header to zero.                                                                                 |
| DELETE |                                                                                                                                      Used for deleting resources.                                                                                                                                      |

</Block>
