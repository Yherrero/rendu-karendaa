<Block>

# events

</Block>
<!-- GET EVENTS/ -->

<Block>

## (get) events/

You can use this API to get all events from one user.  
(auth needed)

### Endpoint

```bash
GET /events
```

### Response

```json
Status: 200

"events": [
      {
        "_id": 1,
        "title": "my title",
        "description": "description",
        "start": 12376158254700,
        "end": 12376168254700,
      }
    ]
```

<Example>

<CURL>
```bash
curl -X GET https://karendaa-api.herokuapp.com/events/
```
</CURL>

</Example>

</Block>

<!-- GET EVENTS/:id -->

<Block>

## (get) events/:eventId

You can use this API to get one event from id.  
(auth needed)

### Endpoint

```bash
GET /events/:eventId
```

### Response

```json
Status: 200

"event":{
        "_id": 1,
        "title": "my title",
        "description": "description",
        "start": 12376158254700,
        "end": 12376168254700,
      }
```

<Example>

<CURL>
```bash
curl -X GET https://karendaa-api.herokuapp.com/events/1
```
</CURL>

</Example>

</Block>

<!-- POST EVENTS/ -->

<Block>

## (post) events/

You can use this API to create new events.  
(auth needed)

### Endpoint

```bash
POST /events
```

|    Name     |  Type  | Description |      Required      |
| :---------: | :----: | :---------: | :----------------: |
|    title    | string |    title    | :heavy_check_mark: |
| description | string | description | :heavy_minus_sign: |
|    start    | number |    start    | :heavy_check_mark: |
|     end     | number |     end     | :heavy_check_mark: |

### Response

```json
Status: 200

"newEvent": {
        "_id": 2,
        "title": "my title",
        "description": "description",
        "start": 12376158254700,
        "end": 12376168254700,
      }
```

<Example>

<CURL>
```bash
curl -X POST https://karendaa-api.herokuapp.com/events/ \
  --data '{
    "title": "my title",
    "description": "description",
    "start": 12376158254700,
    "end": 12376168254700
  }'
```
</CURL>

</Example>

</Block>

<!-- PATCH EVENTS/:id -->

<Block>

## (patch) events/:eventId

You can use this API to edit an events.  
(auth needed)

### Endpoint

```bash
PATCH /events/:eventId
```

|    Name     |  Type  | Description |      Required      |
| :---------: | :----: | :---------: | :----------------: |
|    title    | string |    title    | :heavy_check_mark: |
| description | string | description | :heavy_check_mark: |
|    start    | number |    start    | :heavy_check_mark: |
|     end     | number |     end     | :heavy_check_mark: |

### Response

```json
Status: 200

"newEvent": {
        "_id": 2,
        "title": "my title",
        "description": "description",
        "start": 12376158254700,
        "end": 12376168254700,
      }
```

<Example>

<CURL>
```bash
curl -X PATCH https://karendaa-api.herokuapp.com/events/:eventId \
  --data '{
    "title": "my title edited",
    "description": "description",
    "start": 12376158254700,
    "end": 12376168254700
  }'
```
</CURL>

</Example>

</Block>

<!-- DELETE EVENTS/:id -->

<Block>

## (delete) events/:eventId

You can use this API to delete an events.  
(auth needed)

### Endpoint

```bash
DELETE /events/:eventId
```

### Response

```json
Status: 200

"message": "Event deleted"
```

<Example>

<CURL>
```bash
curl -X DELETE https://karendaa-api.herokuapp.com/events/:eventId
```
</CURL>

</Example>

</Block>
