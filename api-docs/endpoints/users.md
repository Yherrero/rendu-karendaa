<Block>

# users

</Block>

<!-- POST USERS/LOGIN -->

<Block>

## (post) users/login

You can use this API to login the application with your account.

### Endpoint

```bash
POST /users/signup
```

### Parameters

|   Name   |  Type  | Description |      Required      |
| :------: | :----: | :---------: | :----------------: |
|  email   | string |  username   | :heavy_check_mark: |
| password | string |  password   | :heavy_check_mark: |

### Response

```json
Status: 200

{
    "message": "Auth successful",
    "token": "token"
}
```

<Example>

<CURL>
```bash
curl -X POST https://karendaa-api.herokuapp.com/users/login \
  --data '{
    "email": "my@mail.com",
    "password": "my-password"
  }'
```
</CURL>

</Example>

</Block>

<!-- POST USERS/SIGNUP -->

<Block>

## (post) users/signup

You can use this API to create new users.

### Endpoint

```bash
POST /users/login
```

### Parameters

|   Name   |  Type  | Description |      Required      |
| :------: | :----: | :---------: | :----------------: |
|  email   | string |  username   | :heavy_check_mark: |
| password | string |  password   | :heavy_check_mark: |

### Response

```json
Status: 200

"newUser": {
        "_id": 1,
        "email": "my@mail.com",
        "password": "password_hash",
        "calendars": []
    }
```

<Example>

<CURL>
```bash
curl -X POST https://karendaa-api.herokuapp.com/users/signup \
  --data '{
    "email": "my@mail.com",
    "password": "my-password"
  }'
```
</CURL>

</Example>

</Block>

<!-- GET USER/ -->

<Block>

## (get) users/

You can use this API to retrieve your informations as a logged user.  
(auth needed)

:::warning
Authentication needed
:::

### Endpoint

```bash
GET /users/
```

### Response

```json
Status: 200

{
    "user": {
        "_id": 0,
        "email": "yannick@mail.com",
        "password": "$2a$10$UR9BUxZkvMVhMASJcQfHruMAMeoWwZezxGE2Nr7zDSo3Nl1.NgqiO",
        "calendars": [
            {
                "_id": 0,
                "title": "Calendar 1",
                "color": "#bada55",
                "sharedWith": [],
                "events": [
                    {
                        "_id": 0,
                        "title": "Event 1",
                        "description": "",
                        "start": 1561626000000,
                        "end": 1561629600000
                    }
                ]
            }
        ]
    }
}
```

<Example>

<CURL>
```bash
curl -X GET https://karendaa-api.herokuapp.com/users/
```
</CURL>

</Example>

</Block>
