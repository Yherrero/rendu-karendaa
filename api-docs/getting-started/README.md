# Getting Started

- [Users](/endpoints/users.html)
  - signup
  - login
  - retrieve
- [Calendars](/endpoints/calendars.html)
  - create
  - edit
  - delete
- [Events](/endpoints/events.html)
  - get all
  - get one
  - create
  - edit
  - delete
