---
home: true
title: カレンダー API
description: RESTful api for カレンダー app. To get started, click bellow.
actionText: Getting Started
actionLink: /getting-started/
footer: Open Source on [GitLab](https://gitlab.com/Yherrero/karendaa-v2), Made by [@Yannick Herrero](https://yannick-herrero.me) and [@David Seynaeve](https://david-sey.fr), Power by [vuepress](https://github.com/vuejs/vuepress).
---
