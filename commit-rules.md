# Règles de nommage des commits

```
<type>(<portée>): <sujet>

<description>

<footer>
```


* Type définit le type de commit
  * build: Système de build (example : gulp, webpack, npm)
  * ci: Intégration continue (example scopes: Travis, Circle, BrowserStack, SauceLabs)
  * docs: Documentation
  * feat: Ajout d'une fonctionnalité
  * fix: Correction de bogue
  * perf: Amélioration des performances
  * refactor: Changement du code qui ne change rien au fonctionnement
  * style: Changement du style du code (sans changer la logique)
  * test: Modification des tests
* Portée définit quelle partie de votre librairie / application est affectée par le commit (cette information est optionnelle)
* Sujet contient une description succinte des changements
  * En utilisant l'impératif présent ("change", et non pas "changed" ou "changes")
  * Sans majuscule au début
  * Pas de "." à la fin de la description
* Description permet de détailler plus en profondeur les motivations derrière le changement. Les règles sont les mêmes que pour la partie Sujet.
* Footer contient les changements importants (Breaking Changes) et les références à des issues GitHub / GitLab ou autre.
